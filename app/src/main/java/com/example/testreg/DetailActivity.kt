package com.example.testreg

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        nameTextView.text=intent.getStringExtra("name")
        contentAdultTextView.text=intent.getBooleanExtra("contentAdult",false).toString()
        ratingsTextView.text=intent.getStringArrayListExtra("ratings").toString()
        yearTextView.text=intent.getStringExtra("year")
    }
}