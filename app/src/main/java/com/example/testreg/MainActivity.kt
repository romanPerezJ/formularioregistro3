package com.example.testreg

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var contentAdultSelected:Boolean=false
    var ratings:ArrayList<String> = arrayListOf()

    var onChekedChangeListenerRating: CompoundButton.OnCheckedChangeListener=CompoundButton.OnCheckedChangeListener{ buttonView, isChecked ->
        setRating(buttonView)

    }

    var yearsSelected:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //implementacion RadioGroup
        answerContentRadioGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            contentAdultSelected = when (checkedId) {
                R.id.positiveRadioButton -> {
                    true

                }
                R.id.negativeRadioButton -> {
                    false

                }
                else -> {
                    false

                }

            }

        })

        //implementacion CheckBox
        //Agregar Evento
        actionCheckBox.setOnCheckedChangeListener(onChekedChangeListenerRating)
        comedyCheckBox.setOnCheckedChangeListener(onChekedChangeListenerRating)
        dramaCheckBox.setOnCheckedChangeListener(onChekedChangeListenerRating)

        val itemsYears=resources.getStringArray(R.array.years)

        val yearsAdapter = ArrayAdapter<String>(baseContext, android.R.layout.simple_spinner_item, itemsYears)

        yearsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        yearSpinner.adapter = yearsAdapter

        //implementar yearSpinner () su funcionalidad hasta aqui clase 16 enero
        //yearSpinner.setOnItemClickListener()

        yearSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long) {
                yearsSelected = itemsYears[position]

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                yearsSelected = ""
            }

        }
        nameMovieTextInputLayout.editText?.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length!! > nameMovieTextInputLayout.counterMaxLength) {
                    nameMovieTextInputLayout.error = "la cadena a llegado a su limite"

                } else {
                    nameMovieTextInputLayout.error = ""
                    nameMovieTextInputLayout.isErrorEnabled = false
                }
            }
            override fun afterTextChanged(s: Editable?) {

            }


        })

        //implementar Button guardar para ver el contenido
        saveButton.setOnClickListener {
            //sino esta vacio
            if (nameMovieTextInputLayout.isErrorEnabled) {
                Log.d("Error","Entraste error")
                Toast.makeText(baseContext, "Corrige el nombre de la pelicula", Toast.LENGTH_SHORT).show()
            } else {
                val detailIntent = Intent(this, DetailActivity::class.java).apply {
                    val detailBundle = Bundle().apply {
                        putString("name", nameMovieTextInputLayout.editText?.text.toString())
                        putBoolean("contentAdult", contentAdultSelected)
                        putStringArrayList("ratings", ratings)
                        putString("year", yearsSelected)
                    }
                    putExtras(detailBundle)
                }
                startActivity(detailIntent)


            }

        }

    }

    private fun setRating(button: CompoundButton){
        val text=button.text.toString()
        if (ratings.contains(text)) ratings.remove(text) else ratings.add(text)
    }
}